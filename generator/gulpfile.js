var gulp = require('gulp');
var ejs = require('gulp-ejs');

var nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var gulpIf = require('gulp-if');
//var replace = require('gulp-replace');

var replace = require('gulp-replace-path');

var prettify = require('gulp-html-prettify');

var path = require('path');

gulp.task('layout', function () {
    return gulp.src("./views/_layout.html")
        .pipe(ejs({
            body: '%%yield%%'
        }))
        .pipe(gulp.dest("../dist"));
});

gulp.task('compile', ['layout'], function () {
    var layout = require('gulp-file-wrapper');
    gulp.src([
         "./views/[^_]*.html",
         "./views/[^_]*/*.html"
     ])
        .pipe(ejs())
        .pipe(layout('../dist/_layout.html'))
				.pipe(replace(/\/stylesheets/g, function (match, __absolutePath__) {					
						var dirname = path.dirname(path.relative("./views", __absolutePath__));
						if(dirname==".") {
							return '../resources/stylesheets'
						} else {
							return '../../resources/'+'stylesheets'						
						}
					}
				))
				.pipe(replace(/\/images/g, function (match, __absolutePath__) {					
						var dirname = path.dirname(path.relative("./views", __absolutePath__));
						if(dirname==".") {
							return '../resources/images'
						} else {
							return '../../resources/'+'images'						
						}
					}
				))		
				.pipe(replace(/\/js/g, function (match, __absolutePath__) {					
						var dirname = path.dirname(path.relative("./views", __absolutePath__));
						if(dirname==".") {
							return '../resources/js'
						} else {
							return '../../resources/'+'js'						
						}
					}
				))				
			  .pipe(prettify({indent_char: ' ', indent_size: 2}))		
        .pipe(gulp.dest("../static/wechat"));

    gulp.src([
//         "./public/**",
				   "./public/[^_]*/**",		
     ])
		.pipe(gulpIf('*.css',replace(/\/images\//g, '../images/')))
    .pipe(gulp.dest("../static/resources"));
});

gulp.task('dist', ['layout'], function () {
    var layout = require('gulp-file-wrapper');
    gulp.src([
         "./views/[^_]*.html",
         "./views/[^_]*/*.html"
     ])
        .pipe(ejs())
        .pipe(layout('../dist/_layout.html'))
				.pipe(replace(/\/stylesheets/g, function (match, __absolutePath__) {					
						var dirname = path.dirname(path.relative("./views", __absolutePath__));
						if(dirname==".") {
							return 'public/stylesheets'
						} else {
							return '../public/'+'stylesheets'						
						}
					}
				))
				.pipe(replace(/\/images/g, function (match, __absolutePath__) {					
						var dirname = path.dirname(path.relative("./views", __absolutePath__));
						if(dirname==".") {
							return 'public/images'
						} else {
							return '../public/'+'images'						
						}
					}
				))		
				.pipe(replace(/\/js/g, function (match, __absolutePath__) {					
						var dirname = path.dirname(path.relative("./views", __absolutePath__));
						if(dirname==".") {
							return 'public/js'
						} else {
							return '../public/'+'js'						
						}
					}
				))				
			  .pipe(prettify({indent_char: ' ', indent_size: 2}))		
        .pipe(gulp.dest("../dist"));

    gulp.src([
//         "./public/**",
				   "./public/[^_]*/**",		
     ])
		.pipe(gulpIf('*.css',replace(/\/images\//g, '../images/')))
    .pipe(gulp.dest("../dist/public"));
});

var paths = {
    client: [
		"views/**",
    'public/[^_]**/*.*'
    ],
    less: 'public/_less/[^_]**',
    server: {
        index: 'server.js'
    }
};

gulp.task('browser-sync', ['nodemon'], function () {    
    browserSync({
        proxy: "localhost:3000", // local node app address
        port: 11000, // use *different* port than above
        notify: true
    });
    
    return gulp.watch(paths.client, function (e) {
        console.log(e);
        browserSync.reload();
    });

});
          
var nodemonConfig = {
    script : paths.server.index,
    ignore : [
        "dist/**",
        "node_modules/**",
        "views/**",
        "gulpfile.js"
    ],
    env    : {
        "NODE_ENV": "development"
    }
};

gulp.task('nodemon',function (callback) {
    var called = false;
    return nodemon(nodemonConfig)
    .on('start', function() {
        if (!called) {
            called = true;
            callback();
        }
    })
    .on('restart', function() {
        setTimeout(function() {
            reload({
                stream: false
            });
        }, 400);
    });    
});
                    
gulp.task('less',function(){
   var less = require('gulp-less');
   var plumber = require('gulp-plumber'),
       notify = require('gulp-notify');
    
   gulp.src("public/_less/*")
        .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
        .pipe(less())
        .pipe(plumber.stop())
        .pipe(gulp.dest("public/stylesheets"));
});

gulp.task('style',function() {
    return gulp.watch(paths.less,['less']);    
})

gulp.task('default', ['style','browser-sync']);