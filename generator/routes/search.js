var express = require('express');
var router = express.Router();
var path =require('path');

router.get('/', function(request, response, next) {
  response.render('search/index.html');
});

router.get('/*', function(request, response, next) {
  var fileName = path.basename(request.url,'.html');
	console.log(fileName);
  response.render('search/'+fileName);
});

module.exports = router;
