var express = require('express');
var router = express.Router();
var path =require('path');

router.get('/', function(request, response, next) {
  response.render('statement/index.html');
});

router.get('/*', function(request, response, next) {
  var fileName = path.basename(request.url,'.html');
	console.log(fileName);
  response.render('statement/'+fileName);
});

module.exports = router;
