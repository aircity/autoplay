var express = require('express');
var router = express.Router();
var path =require('path');

router.get('/', function(request, response, next) {
  response.render('index');
});

//[^search]*

router.get('*', function(request, response, next) {
  var fileName = path.basename(request.url,'.html');
  response.render(fileName);
});

module.exports = router;
